
const api = {
  secret_key: "c81e728d9d4c2f636f067f89cc14862c",
  endpoint: "http://maat.trust.engineering:8080/ords/maat/",
  version: "v1",
  modules: {
    serverRegisterData: "/server/register/",
    aliveGetData: "/alive",
    aliveRegisterData: "/alive/register/",
    serverProcServices: "/server/hash_id/proc_services",
    taskSchedule: "/server/hash_id/task_schedule"
  }
}

const sck = {
  endpoint: "http://maat.trust.engineering:8721",
  channel: "maat"
}

module.exports = {
  api, sck
}
