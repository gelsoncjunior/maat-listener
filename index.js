const sleep = require('system-sleep')
const sckon = require('./sckon')
const core = {
  srv: require('./src/controller/serverController'),
  alive: require('./src/controller/aliveController'),
  util: require('./src/util/util')
}

async function server() {
  core.srv.system()
  core.srv.cpuLoad()
  core.srv.filesystem()
  core.srv.memory()
  core.srv.cpuProcess()
  core.srv.network()
  core.srv.services()
  //Only Win32
  core.srv.winTaskSchedule()
}

async function alive() {
  core.alive.alive()
}

function main() {
  server()
  alive()
}

while (true) {
  main()
  sleep(60000)
}

