const fs = require('fs')
const secret_key = process.argv[2]
const maat_server = process.argv[3] || 'maat.trust.engineering'

const content = `
const api = {
  secret_key: "${secret_key}",
  endpoint: "http://${maat_server}:8080/ords/maat/",
  version: "v1",
  modules: {
    serverRegisterData: "/server/register/",
    aliveGetData: "/alive",
    aliveRegisterData: "/alive/register/",
    serverProcServices: "/server/hash_id/proc_services",
    taskSchedule: "/server/hash_id/task_schedule"
  }
}

const sck = {
  endpoint: "http://${maat_server}:8721",
  channel: "maat"
}

module.exports = {
  api, sck
}
`



fs.writeFile('config/cfg.js', content, (err) => {
  if (!err) {
    console.log("Init configured!")
  }
})