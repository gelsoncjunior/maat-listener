const axios = require('axios')
const { api } = require('../../config/cfg')
const tcp = require('../util/ping')
const conn = require("../util/connect")

async function getAllAddressToAlive() {
  return new Promise((resolve, reject) => {
    axios({
      method: "GET",
      url: api.endpoint + api.version + api.modules.aliveGetData + "/" + api.secret_key
    }).then((data) => {
      resolve(data)
    }).catch(e => {

    })
  })
}

async function alive() {
  const category = "alive"
  function aliveStatus(ipAddress, portCon, alive_id) {
    tcp.ping(
      {
        address: ipAddress,
        port: portCon,
        timeout: 3000
      },
      (err, data) => {
        out = {
          address: data.address,
          port: data.port,
          avg: data.avg,
          max: data.max,
          min: data.min,
          status: data.status
        }
        data = JSON.stringify(out)
        conn.sent({
          method: "POST",
          url: api.endpoint + api.version + api.modules.aliveRegisterData,
          data: {
            alive_id: alive_id,
            data_json: data,
          }
        }, category)
      }
    );
  }

  let ip_list = await getAllAddressToAlive()
  if (ip_list.data.items.length != 0) {
    for (let i of ip_list.data.items) {
      aliveStatus(i.endpoint, i.port_listener, i.id)
    }
  }
}


module.exports = { alive }