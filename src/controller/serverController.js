const si = require('systeminformation')
const { api } = require('../../config/cfg')
const conn = require('../util/connect')
const util = require('../util/util')

async function system() {
  const category = "informationSystem"
  let infoSystem = await si.getStaticData()
  let infoUptime = await si.time().uptime

  let data = {
    platform: infoSystem.os.platform,
    distro: infoSystem.os.distro,
    release: infoSystem.os.release,
    kernel: infoSystem.os.kernel,
    arch: infoSystem.os.arch,
    hostname: infoSystem.os.hostname,
    codepage: infoSystem.os.codepage,
    serial: infoSystem.os.serial,
    manufacturer: infoSystem.system.manufacturer,
    uptime: infoUptime
  }

  data = JSON.stringify(data)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)

}

async function cpuLoad() {
  const category = "cpuSystem"
  let cpu = await si.currentLoad();
  data = JSON.stringify(cpu)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)
}

async function filesystem() {
  const category = "fileSystem"
  let infoDisk = await si.fsSize();
  data = JSON.stringify(infoDisk)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)
}

async function memory() {
  const category = "memorySystem"
  let infoMemory = await si.mem();
  let data = JSON.stringify(infoMemory)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)
}

async function network() {
  const category = "networkSystem"
  let infoNetwork = await si.networkInterfaces();
  let data = JSON.stringify(infoNetwork)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)
}

async function cpuProcess() {
  const category = "cpuProcessSystem"
  let infoCpuProcess = await si.processes();
  let data = JSON.stringify(infoCpuProcess)
  conn.sent({
    method: "POST",
    url: api.endpoint + api.version + api.modules.serverRegisterData,
    data: {
      data_json: data,
      category: category,
      secret_key: api.secret_key
    }
  }, category)
}

async function services() {
  const category = "serviceSystem"
  let serviceList = await conn.sent({
    method: "GET",
    url: api.endpoint + api.version + api.modules.serverProcServices.replace("hash_id", api.secret_key),
  }, category)

  if (serviceList.items.length != 0) {
    let arryService = []
    for (let service of serviceList.items) {
      let infoService = await si.services(service.service_name);
      arryService.push(infoService[0])
    }
    conn.sent({
      method: "POST",
      url: api.endpoint + api.version + api.modules.serverProcServices.replace("hash_id", api.secret_key),
      data: {
        data_json: JSON.stringify(arryService),
        category: category
      }
    }, category)
  }
}

async function winTaskSchedule() {
  if (util.thisPlatform() == 'win32') {
    const category = "taskScheduler"
    let taskSchedulerList = await conn.sent({
      method: "GET",
      url: api.endpoint + api.version + api.modules.taskSchedule.replace("hash_id", api.secret_key),
    }, category)

    if (taskSchedulerList.items.length != 0) {
      const arrayTask = []
      for (let i of taskSchedulerList.items) {
        let { error, stdout, stderr } = await util.execCommand(`schtasks /Query /V /FO LIST /TN "${i.taskschedule_path}"`)
        if (error || stderr) {
          console.log('err', error)
          console.log('err', stderr)
        } else {
          let lines = stdout.split('\r\n')
          let task = {
            folder: util.getValue(lines, "Folder", ":", true),
            hostname: util.getValue(lines, "HostName", ":", true),
            taskname: util.getValue(lines, "TaskName", ":", true),
            next_run_time: util.getValue(lines, "Next Run Time", ":", true),
            status: util.getValue(lines, "Status", ":", true),
            logon_mode: util.getValue(lines, "Logon Mode", ":", true),
            last_run_time: util.getValue(lines, "Last Run Time", ":", true),
            task_to_run: util.getValue(lines, "Task To Run", ":", true),
            scheduled_task_state: util.getValue(lines, "Scheduled Task State", ":", true),
            days: util.getValue(lines, "Days", ":", true),
            months: util.getValue(lines, "Months", ":", true),
            repeat_every: util.getValue(lines, "Repeat: Every", ":", true).replace('Every:', "").trim(),
            repeat_until_time: util.getValue(lines, "Repeat: Until: Time", ":", true).replace('Until: Time:', "").trim(),
            repeat_until_duration: util.getValue(lines, "Repeat: Until: Duration", ":", true).replace('Until: Duration:', "").trim(),
            repeat_stop_if_still_running: util.getValue(lines, "Repeat: Stop If Still Running", ":", true).replace('Stop If Still Running:', "").trim()
          }
          arrayTask.push(task)
        }

      }
      conn.sent({
        method: "POST",
        url: api.endpoint + api.version + api.modules.taskSchedule.replace("hash_id", api.secret_key),
        data: {
          data_json: JSON.stringify(arrayTask),
          category: category
        }
      }, category)
    }
  }
}

module.exports = {
  system, cpuLoad, filesystem, memory, network, cpuProcess, services, winTaskSchedule
}