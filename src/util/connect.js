const axios = require('axios')
const { code_erro } = require('../util/tools')

async function sent(options, category) {
  return new Promise((resolve, reject) => {
    axios(options).then((res) => {
      console.log({ status: res.status, data: `Tramission data sent: ${category}` })
      if (res.status === 200) {
        resolve(res.data)
      } else {
        console.log("Failed")
      }
    }).catch((err) => {
      console.log(err)
    })
  })
}

module.exports = { sent }