const fs = require("fs")

const fileJson = "dataJson/data.json"

function code_erro(code) {
  switch (code) {
    case 200:
      return "OK - Sucesso"
    case 400:
      return "Bad Request - Requisição inválida"
    case 401:
      return "Unauthorized - Chave de API inválida"
    case 404:
      return "Not Found - O recurso solicitado não existe"
    case 412:
      return "Precondition Failed - Parâmetros validos mas a requisição falhou"
    case 422:
      return "Unprocessable Entity - Parâmetros inválidos"
    case 500:
      return "Internal Server Error - Ocorreu um erro interno"
    default:
      return "Codigo nao identificado"
  }
}

function statusLatencyReturn(avg) {
  if (avg > 0 && avg < 300) {
    return status = 1;
  } else if (avg > 300) {
    return status = 2;
  } else if (avg == 0) {
    return status = 3;
  } else {
    return status = 4;
  }
}

function cleanDataJson() {
  fs.writeFile(fileJson, "", (err) => {
    if (!err) {
      console.log('info', 'Clean dataJson executed.')
    } else {
      console.log('crit', 'Failed when trying executing cleanDataJson.')
    }
  })
}

async function transformDataJson() {
  return new Promise((resolve, reject) => {
    fs.readFile(fileJson, (err, buffer) => {
      if (!err) {
        let transformEstructureJson = buffer.toString().replace(/}{/g, "},{").replace(/}}/g, "}")
        let bufferJson = `[${transformEstructureJson}]`
        bufferJson = JSON.parse(bufferJson)
        checkStatus = checkSearchString(JSON.stringify(bufferJson), '"status":1')
        if (checkStatus === -1) {
          log('crit', `Connection with endpoints failed.`)
          bufferJson = []
          resolve(bufferJson)
        } else {
          log('info', `Connection with endpoint alive.`)
          console.log(JSON.stringify(bufferJson))
          resolve(bufferJson)
        }
      } else {
        log('err', err)
      }
    })
  })
}

function writeDataJson(data) {
  fs.appendFile(fileJson, data, (err) => { })
}

module.exports = { code_erro, statusLatencyReturn, cleanDataJson, writeDataJson, transformDataJson }